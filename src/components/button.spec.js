import * as React from 'react';
import { mount } from '@cypress/react'
import Button from './button'

describe('Button elements', () => {
  beforeEach(() => {
    mount(<Button />)
  })

  it('All elements rendered', () => {
    cy.get('#demo-button-outlined').should('be.visible')
    cy.get('#demo-button-disabled').should('be.visible')
    cy.get('#demo-button-link').should('be.visible')
    cy.get('#demo-button-click').should('be.visible')
  })
  
  it('Primary button contains expected text', () => {
    cy.get('#demo-button-outlined').should('have.text', 'Primary')
  })
  
  it('Disabled button disabled in DOM', () => {
    cy.get('#demo-button-disabled').should('be.disabled')
  })
  
  it('Link button contains expected link', () => {
    // Click is disabled as the iframe is rendered on top of the component unless forced 
    cy.get('#demo-button-link').click({ force: true })

    // TODO: Figure out how to assert on the hashchange event
    // cy.on('url:changed', (url) => {
    //   cy.location('hash').then(hash => {
    //       if (!url.endsWith(hash)) {
    //           cy.log(url);
    //       }
    //   });
    // });
  })
  
  it('Click button creates alert', () => {
    mount(<Button />)
    // Click is disabled as the iframe is rendered on top of the component unless forced 
    cy.get('#demo-button-click').click({ force: true })
    cy.on('window:alert', (text) => {
      // window:alert is the event which gets fired on alert open
      expect(text).to.eql('clicked')
    })
  })
})
