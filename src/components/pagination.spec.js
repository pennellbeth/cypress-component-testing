import * as React from 'react';
import { mount } from '@cypress/react'
import PaginationRounded from './pagination'

it('Render the pagination components with default values', () => {
  mount(<PaginationRounded />)
  cy.get('#standard-pages').should('be.visible')
  cy.get('#standard-pages>ul>li').find('button').then((buttons) => {
    expect(buttons).to.have.length(8)
    expect(buttons[1]).to.have.attr('aria-label', 'page 1')
    expect(buttons[1]).to.have.attr('aria-current', 'true')
  })

  cy.get('#outlined-pages').should('be.visible')
  cy.get('#outlined-pages>ul>li').find('button').then((buttons) => {
    expect(buttons).to.have.length(8)
    expect(buttons[1]).to.have.attr('aria-label', 'page 1')
    expect(buttons[1]).to.have.attr('aria-current', 'true')
  })

  cy.get('#first-last-buttons').should('be.visible')
  cy.get('#first-last-buttons>ul>li').find('button').then((buttons) => {
    expect(buttons).to.have.length(10)
    expect(buttons[2]).to.have.attr('aria-label', 'page 1')
    expect(buttons[2]).to.have.attr('aria-current', 'true')
  })

  cy.get('#no-first-last-buttons').should('be.visible')
  cy.get('#no-first-last-buttons>ul>li').find('button').then((buttons) => {
    expect(buttons).to.have.length(6)
    expect(buttons[0]).to.have.attr('aria-label', 'page 1')
    expect(buttons[0]).to.have.attr('aria-current', 'true')
  })
})

it('Can change to another specified page', () => {
  mount(<PaginationRounded />)
  cy.get('#standard-pages>ul>li').eq(3).click()
    .then((p3) => {
      cy.wrap(p3).find('button').should('have.attr', 'aria-current', 'true')
    })
})

it('Pages adjacent to ellipsis return nearby pages', () => {
  mount(<PaginationRounded />)
  cy.get('#outlined-pages>ul>li').eq(5).click()
  cy.get('#outlined-pages>ul>li').then((list) => {
    cy.wrap(list[2]).find('div').should('have.class', 'MuiPaginationItem-ellipsis')
    cy.wrap(list[6]).find('div').should('have.class', 'MuiPaginationItem-ellipsis')
    cy.get('#outlined-pages>ul>li>button').should((buttons) => {
      expect(buttons[2]).to.have.attr('aria-label', 'Go to page 4')
      expect(buttons[4]).to.have.attr('aria-label', 'Go to page 6')
    })
  })
})

it('Can navigate using forward and backwards icon', () => {
  mount(<PaginationRounded />)
  cy.get('[data-testid="NavigateNextIcon"]').eq(1).click()
  cy.get('#outlined-pages>ul>li').eq(2).then((p2) => {
    cy.wrap(p2).find('button').should('have.attr', 'aria-current', 'true')
  })
  cy.get('[data-testid="NavigateBeforeIcon"]').eq(1).click()
  cy.get('#outlined-pages>ul>li').eq(1).then((p1) => {
    cy.wrap(p1).find('button').should('have.attr', 'aria-current', 'true')
  })
})

it('Disables forward and backwards icons at page extremities', () => {
  mount(<PaginationRounded />)
  cy.get('#standard-pages>ul>li').eq(0).then(($ele) => {
    cy.wrap($ele).find('button').should('be.disabled')
  })
  cy.get('#standard-pages>ul>li').eq(7).click()
  cy.get('#standard-pages>ul>li').eq(8).then(($ele) => {
    cy.wrap($ele).find('button').should('be.disabled')
  })
})

it('Can navigate using first and last page icons', () => {
  mount(<PaginationRounded />)
  cy.get('[data-testid="LastPageIcon"]').click()
  cy.get('#first-last-buttons>ul>li').eq(8).then((p10) => {
    cy.wrap(p10).find('button').should('have.attr', 'aria-current', 'true')
  })
  cy.get('[data-testid="FirstPageIcon"]').click()
  cy.get('#first-last-buttons>ul>li').eq(2).then((p1) => {
    cy.wrap(p1).find('button').should('have.attr', 'aria-current', 'true')
  })
})

it('Disables first and last icons at page extremities', () => {
  mount(<PaginationRounded />)
  cy.get('[data-testid="FirstPageIcon"]').parent().should('be.disabled')
  cy.get('#first-last-buttons>ul>li>button').eq(7).click()
  cy.get('[data-testid="LastPageIcon"]').parent().should('be.disabled')
})