import * as React from 'react';
import { mount } from '@cypress/react'
import AnchorElTooltips from './hoverTooltip'

it('Render the base component', () => {
  mount(<AnchorElTooltips />)
  cy.get('#box-area').should('have.contain', 'Hover')
})

it('Renders the tooltip on mouseover', () => {
  mount(<AnchorElTooltips />)
  cy.get('[role="tooltip"]').should('not.exist')
  cy.get('#box-area').trigger('mouseover')
  cy.get('[role="tooltip"]').should('be.visible')
    .and('have.contain', 'Add')
})