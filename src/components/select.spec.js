import * as React from 'react';
import { mount } from '@cypress/react'
import BasicSelect from './select'

describe('Select dropdown menu components', () => {
  beforeEach(() => {
    mount(<BasicSelect />)
  })

  context('Default rendering and elements', () => {
    it('Renders all select menus', () => {
      cy.get('#demo-simple-select-label').should('have.contain', 'Age')
      cy.get('#demo-simple-select-required-label').should('have.contain', 'Age')
      cy.get('#demo-simple-select-disabled-label').should('have.contain', 'Gender')
      cy.get('#demo-simple-select-error-label').should('have.contain', 'Food')
      cy.get('#demo-simple-select-readonly-label').should('have.contain', 'Drink')
    })
    
    it('Main select list contains expected items', () => {
      cy.get('#demo-simple-select').click().then(() => {
        cy.get('[role="listbox"]').children()
        .should((list) => {
          expect(list).to.have.length(3)
          expect(list[0]).to.contain('Forty')
          expect(list[1]).to.contain('Fifty')
          expect(list[2]).to.contain('Sixty')
        })
      })
    })

    it('Required list contains expected items', () => {
      cy.get('#demo-simple-select-required').click().then(() => {
        cy.get('[role="listbox"]').children()
        .should((list) => {
          expect(list).to.have.length(3)
          expect(list[0]).to.contain("Ten")
          expect(list[1]).to.contain("Twenty")
          expect(list[2]).to.contain("Thirty")
        })
      })
    })
  })

  context('Styling', () => {
    it('Disabled list has correct styling', () => {
      cy.get('#demo-simple-select-disabled-label')
        .should('have.css', 'color', 'rgba(0, 0, 0, 0.38)')
      cy.get('#demo-simple-select-disabled').siblings('.MuiOutlinedInput-notchedOutline')
        .should('have.css', 'border-color', 'rgba(0, 0, 0, 0.26)')
      cy.get('#demo-simple-select-disabled-label').siblings('p')
        .should('have.css', 'color', 'rgba(0, 0, 0, 0.38)')
    })

    it('Error dropdown has correct styling', () => {
      cy.get('#demo-simple-select-error-label')
        .should('have.css', 'color', 'rgb(211, 47, 47)')
      cy.get('#demo-simple-select-error').siblings('.MuiOutlinedInput-notchedOutline')
        .should('have.css', 'border-color', 'rgb(211, 47, 47)')
      cy.get('#demo-simple-select-error-label').siblings('p')
        .should('have.css', 'color', 'rgb(211, 47, 47)')
    })
  })

  context('Selection and dropdown state', () => {
    it('Main select list can select an item', () => {
      cy.get('#demo-simple-select').click()
      cy.get('li[role="option"]').each(function ($ele, index, list) {
        if($ele.text() === 'Sixty') {
          cy.log("Found option: ", $ele.text())
          cy.wrap($ele).click()
        } else {
          cy.log("Current value: ", $ele.text())
        }
      })
      cy.get('#demo-simple-select').should('have.contain', 'Sixty')
    })
    
    it('Required list can select an item', () => {
      cy.get('#demo-simple-select-required').click()
      cy.get('li[role="option"]').each(function ($ele, index, list) {
        if($ele.text() === 'Twenty') {
          cy.log("Found option: ", $ele.text())
          cy.wrap($ele).click()
        } else {
          cy.log("Current value: ", $ele.text())
        }
      })
      cy.get('#demo-simple-select-required').should('have.contain', 'Twenty')
    })
    
    it('Disabled list is disabled', () => {
      cy.get('#demo-simple-select-disabled').next().should("be.disabled")
    })
    
    it('Read Only list is read only', () => {
      cy.get('#demo-simple-select-readonly-label').next().find('svg')
        .invoke('attr', 'focusable')
        .should('equal', 'false')
    })
  })
})