import * as React from 'react';
import { mount } from '@cypress/react'
import ControlledRadioButtonsGroup from './radioButton'

it('Renders the radio button', () => {
  mount(<ControlledRadioButtonsGroup />)
  cy.get('#demo-controlled-radio-buttons-group').should('have.text', 'Gender')
  cy.get('#button-group').children().should((children) => {
    expect(children).to.have.length(2)
    expect(children[0]).to.have.attr('id', 'option-1')
    expect(children[1]).to.have.attr('id', 'option-2')
  })
  cy.get('#option-1').should('have.contain', 'Female')
  cy.get('#option-2').should('have.contain', 'Male')
})

it('Can select an option', () => {
  mount(<ControlledRadioButtonsGroup />)
  cy.get('#option-2').find('input[type="radio"]', { includeShadowDom: true }).check({ force: true })
    .should("be.checked")
})

it('Only one option can be selected at a time', () => {
  mount(<ControlledRadioButtonsGroup />)
  cy.get('#option-1').find('input[type="radio"]', { includeShadowDom: true })
    .should("be.checked")
  cy.get('#option-2').find('input[type="radio"]', { includeShadowDom: true }).check({ force: true })
    .should("be.checked")
  cy.get('#option-1').find('input[type="radio"]', { includeShadowDom: true })
    .should("not.be.checked")
})
