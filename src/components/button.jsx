import * as React from 'react';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';

export default function OutlinedButtons() {
  return (
    <Stack direction="row" spacing={2}>
      <Button variant="outlined"
        id="demo-button-outlined"
      >
        Primary
      </Button>
      <Button variant="outlined"
        disabled
        id="demo-button-disabled"
      >
        Disabled
      </Button>
      <Button variant="outlined"
        href="#outlined-buttons"
        id="demo-button-link"
      >
        Link
      </Button>
      <Button
        onClick={() => {
          alert('clicked');
        }}
        id="demo-button-click"
      >
        Click me
      </Button>
    </Stack>
  );
}
