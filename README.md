# Cypress Component Testing

This repo explores the potential of Cypress' component testing feature. It uses
standard components from Create React App through the Mui testing library:
<https://mui.com/material-ui/getting-started/overview/>

## Running the tests

Components under test are found in the `/src/components` folder.
Tests are held in `.spec.js` files in the same folder.

Tests are run using the Cypress test runner, either with `npm run test-ui` or
`npx cypress open-ct`
This will open a new browser window where the tests can be viewed and debugged.

You can also run the tests headlessly with `npm run test` or `npx cypress run-ct`

## Notes

- This project is running Cypress v9, which is still the Alpha for component testing.
  The Beta was released in June 2022 with Cypress 10, however this relies on React 18
  and that doesn't play nicely with some of the other Cypress react and webpack packages.
  So the earlier version has been chosen for this repo.

## Known Issues

- /src/components folder in the .eslintignore file - After 3 days of dealing with peer
  dependency hell, packages incompatible with one another and circular dependencies, this
  was the only way to get the flipping thing to render in the Test Runner.
  Permissible for the use case of the POC as eslint isn't the thing under evaluation.
